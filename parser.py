from instruction.instruction import Instruction

class Parser:

  def __isBlank (self, string):
    if string and string.strip():
        # string is not None AND myString is not empty or blank
        return False
    # string is None OR myString is empty or blank
    return True

  def __isComment(self, line):
    if line.strip().startswith("//"):
        return True
    return False

  def __isWhiteSpace(self, line):
    return self.__isBlank(line) or self.__isComment(line)

  def __stripInlineComment(self, line):
    items = line.split()

    if len(items) > 1:
        return items[0]

    return line

  def parse(self, line, ignore_label):
     if not self.__isWhiteSpace(line):
         line = self.__stripInlineComment(line)

         if self.is_label(line):
             if not ignore_label:
                 return self.__get_label_name(line)
             else:
                 return None
         else:
             return Instruction(line)

     return None

  def is_label(self, line):
      return line.startswith('(') and line.endswith(')')

  def __get_label_name(self, line):
      if self.is_label(line):
          return line.strip("()")
      return None
