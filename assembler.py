import sys

from parser import Parser
from translator import Translator
from symbol_table import SymbolTable

class Assembler:
    def __init__(self, filename):
        self.filename = filename

    def assemble(self):

        lines = self.__read_program_file(self.filename)

        # Initialization: of parser and symbol table.
        #
        # Construct an empty symbol table. Add the pre-defined symbols
        # to the symbol table.
        parser = Parser()
        symbol_table = SymbolTable()

        # First pass: read all commands, only paying attention to labels and
        # updating the symbol table.
        self.__first_pass(lines, parser, symbol_table)

        # Second pass: restart reading and translating commands.
        #
        # Main loop:
        # 1. Get the next Assembly Language Command and parse it
        # 2. For A-commands: Translate symbols to binary addresses
        # 3. For C-commands: get code for each part and put them together
        # 4. Output the resulting machine language command
        self.__second_pass(lines, parser, symbol_table)

    def __read_program_file(self, filename):
        program = open(filename, 'r')
        lines = program.readlines()

        return lines

    def __first_pass(self, lines, parser, symbol_table):
        address = 0

        # Scan the entire program;
        for line in lines:
            line = line.strip()
            instruction = parser.parse(line, False)

            if instruction is not None:
                # For each “instruction” of the form (xxx):
                if parser.is_label(line):
                    if symbol_table.contains(instruction) is False:
                        # Add the pair (xxx, address) to the symbol table,
                        # where address is the number of the instruction following (xxx)
                        symbol_table.put(instruction, address)
                else:
                    address = address + 1

    def __second_pass(self, lines, parser, symbol_table):
        binary_file = open(self.filename.replace(".asm", ".hack"), 'w')

        # that's how memory is arranged here. We have R0-R15 registers,
        # so can start from 16
        #
        # Set n to 16
        n = 16

        # Scan the entire program again; for each instruction:
        for line in lines:
            line = line.strip()
            instruction = Parser().parse(line, True)

            if instruction is not None:
                # If the instruction is @symbol, look up symbol in the symbol table;
                if instruction.is_variable():
                    variable = instruction.addr()
                    # If not found:
                    if symbol_table.contains(variable) is False:
                        # Use n to complete the instruction's translation
                        address = n
                        # Add (symbol, 1) to the symbol table
                        symbol_table.put(variable, address)
                        # n++
                        n = n + 1
                    else:
                        # If (symbol, value) is found, use value to complete the instruction's translation;
                        address = symbol_table.get(variable)

                    instruction.replace_with_addr(str(address))

                # If the instruction is a C-instruction, complete the instruction's translation
                binary_instruction = Translator(instruction).translate()
                # Write the translated instruction to the output file.
                binary_file.write(f'{binary_instruction}\n')

        binary_file.close()

def main():
    assembler = Assembler(sys.argv[1])
    assembler.assemble()

if __name__ == "__main__":
    main()
