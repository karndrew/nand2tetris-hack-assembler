from .instruction_type import InstructionType

class Instruction:
    def __init__(self, line):
        self.line = line.strip()

        self.inst_type = None
        self.dest_field = None
        self.comp_field = None
        self.jump_field = None
        self.address = self.__compute_address(line)

        if self.address is None:
            self.__compute_command(line)

    def __compute_address(self, line):
        if line.startswith('@'):
            self.inst_type = InstructionType.A
            return line[1:]
        else:
            return None

    def __compute_command(self, line):
        self.inst_type = InstructionType.C

        dest_comp = line.split('=')
        comp_jump = line.split(';')

        if len(dest_comp) > 1:
            self.dest_field = dest_comp[0]
            self.comp_field = dest_comp[1]
        elif len(comp_jump) > 1:
            self.comp_field = comp_jump[0]
            self.jump_field = comp_jump[1]

    def instruction_type(self):
        return self.inst_type

    def addr(self):
        return self.address

    def replace_with_addr(self, address):
        self.address = address

    def dest(self):
        return self.dest_field

    def comp(self):
        return self.comp_field

    def jump(self):
        return self.jump_field

    def is_variable(self):
        return (self.inst_type is InstructionType.A) and not self.address.isnumeric()
