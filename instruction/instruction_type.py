from enum import Enum

class InstructionType(Enum):
    A = 0
    C = 1
